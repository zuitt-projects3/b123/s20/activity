// We can also have an array of obects. We can group together objects in an array. That array can also
// use array methods for the objects. However, being objects are more complex than strings or numbers
// there is a difference when handling them.

let users = [

{
  name: "Mike Shell",
  username: "mikeBlueShell",
  email: "mikeShelly1101@gmail.com",
  password: "iammikey1999",
  isActive: true,
  datJoined: "August 8, 2011"
},
{

  name: "Jake Janella",
  username: "jjnella99",
  email: "jakejanella_99@gmail.com",
  password: "jakiejake12",
  isActive: true,
  datJoined: "January 14, 2015"

},

];

// Much like a regular array, we can actually acces this array of objects the same way.

console.log(users[0])
console.log(users[1].email)

// Mini Activity

users[0].email = "mikeKingOfShells@gmail.com";
console.log(users[0].email);

users[1].email = "janellajakeArchitect@gmail.com";
console.log(users[1].email);

users.push({
  name: "James Jameson",
  username: "iHateSpidey",
  email: "jamesJjamesongmail.com",
  password: "spideyisamenace64",
  isActive: true,
  datJoined: "February 14, 2000"
})

console.log(users[2]);

class User{
  constructor(name,username,email,password){
    this.name = name;
    this.username = username;
    this.email = email;
    this.password = password;
    this.isActive = true;
    this.dateJoined = "September 28, 2021";
  }
}
let newUser1 = new User("Kate Middletown","notTheDuchess","seriouslyNotDuchess@gmail.com",
"notRoyaltyAtALL")

console.log(newUser1);
users.push(newUser1);
console.log(users);

function login(username,password){
  // check if the username does exist or any user users that username

  let userFound = users.find((user) => {
    // user is a paramete which receives each items in the users array. and the users array
    // is an array of objects. Therefore we can expect that the user parameter will contain
    // an object.
    return user.username === username && user.password === password

  })

  if(userFound){
    alert(`Thank you for Logging in ${userFound.name}!`)
  } else {
    alert("Login Failed, Invalid Credentials")
  }
}

// Activity ----------------------------

// Create

let courses = [];

const postCourse = (id,name,description,price) => {

  this.name = name;
  this.id = id;
  this.price = price;
  this.description = description;
  this.isActive = true;
  let newCourse = [this.name,this.id,this.description,this.price,this.isActive]
  courses.push(newCourse)
  alert(`You have created ${this.name}. The Price is ${this.price}`)
}

postCourse("Differential Equations","Math 010","Introduction to Differential Equations",600);
console.log(courses);
postCourse("Sports Games","PE 004","Introduction to Team Sports",300);


// Retrieve/Read


const getSingleCourse = (id) => {
    let foundCourse = courses.find((course) => id === course.id);
        return (foundCourse);
    }



// Delete

const deleteCourse = (course) => {
  courses.pop(course.name)
  return courses
}
